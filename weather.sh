#!/bin/bash

LOCATIONDISPLAY="Lovisa"

#get weather info
WEATHER="wget -qO- ilmatieteenlaitos.fi/saa/loviisa"

# get temperature
TEMP=$($WEATHER | hxselect -s '\n' -c  'tr.meteogram-temperatures' | hxselect -s '\n' -c 'td' | grep -o -P '(?<=>).*(?=<)' | sed -n 1p)

# get wind
WINDDIRECTION=$($WEATHER | hxselect -s '\n' -c  'tr.meteogram-wind-symbols' | hxselect -s '\n' -c 'td' | grep -o -P '(?<=code-).*(?=-)' | sed -n 1p)

WINDSPEED=$($WEATHER | hxselect -s '\n' -c  'tr.meteogram-wind-symbols' | hxselect -s '\n' -c 'td' | grep -o -P '(?<=tuulta ).*(?=">)' | sed -n 1p)

echo "$LOCATIONDISPLAY: $TEMP $WINDDIRECTION $WINDSPEED" > /home/oscar/.i3/weather
