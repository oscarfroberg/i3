#!/bin/bash

connect () {
    if [ $1 == "VGA1" ]; then
        xrandr --output LVDS1 --auto --primary --output VGA1 --auto --right-of LVDS1
    elif [ $1 == "HDMI1" ]; then
        xrandr --output LVDS1 --auto --primary --output HDMI1 --mode 1920x1080 --rate 60 --right-of LVDS1
        #the following doesn't work with my tv, use it with external monitors if needed
        #xrandr --output LVDS1 --auto --primary --output HDMI1 --auto --right-of LVDS1
    fi
}

disconnect () {
    if [ $1 == "VGA1" ]; then
        xrandr --output LVDS1 --auto --primary --output VGA1 --off
    elif [ $1 == "HDMI1" ]; then
        xrandr --output LVDS1 --auto --primary --output HDMI1 --off
    fi
}

if xrandr --current | grep -q 'VGA1 disconnected'; then
    echo "VGA1 unplugged, turning off just in case"
    disconnect VGA1
elif xrandr --current | grep -q 'VGA1 connected (normal'; then
    echo "VGA1 plugged in, trying to enable..."
    connect VGA1
elif xrandr --current | grep -q 'VGA1 connected'; then
    echo "VGA1 connected, trying to disconnect..."
    disconnect VGA1
fi

if xrandr --current | grep -q 'HDMI1 disconnected'; then
    echo "HDMI1 unplugged, turning off just in case"
    xautolock -enable
    disconnect HDMI1
elif xrandr --current | grep -q 'HDMI1 connected (normal'; then
    echo "HDMI1 plugged in, trying to enable..."
    xautolock -disable
    connect HDMI1
elif xrandr --current | grep -q 'HDMI1 connected'; then
    echo "HDMI1 connected, trying to disconnect..."
    xautolock -enable
    disconnect HDMI1
fi
