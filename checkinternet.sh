#! /bin/sh

host1=google.fi
host2=wikipedia.org
host3=208.67.222.222
statusfile=/home/oscar/.i3/internet
times=0

while [ $times -lt 3 ]; do
    ((ping -w5 -c3 $host1 || ping -w5 -c3 $host2 || ping -w5 -c3 $host3) > /dev/null 2>&1) && echo "✔" > $statusfile || (echo "✗" > $statusfile && exit 1)
    times=`expr $times + 1`
    # sleep which causes the script to run about 3 times per minute (set to every minute in crontab)
    sleep 18
done
